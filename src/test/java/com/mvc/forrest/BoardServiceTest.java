package com.mvc.forrest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mvc.forrest.service.board.BoardService;
import com.mvc.forrest.service.domain.Board;
import com.mvc.forrest.service.domain.Search;


@SpringBootTest
public class BoardServiceTest {

	//==>@RunWith,@ContextConfiguration 이용 Wiring, Test 할 instance DI
//	@Autowired
//	private BoardService boardService;
//
//	@Test
//	public void testListBoard() throws Exception {
//		
//		Search search = new Search();
//		search.setCurrentPage(1);
//		Board board = new Board();
//		board.setBoardFlag("A");
//		
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("board", board);
//		map.put("search", search);
//		
//		System.out.println(boardService.getListBoard(map));
//		assertEquals("tesasdtUserId", boardService.getListBoard(map));
//	}
	
	@Test
	void contextLoads() {
	}

	@Test
	void jasypt() {
		String url = "jdbc:mariadb://db-chamch.site:3306/TEST2?useUnicode=true&characterEncoding=utf8";
		String username = "test2";
		String password = "test21010%%";

		System.out.println(jasyptEncoding(url));
		System.out.println(jasyptEncoding(username));
		System.out.println(jasyptEncoding(password));
	}

	public String jasyptEncoding(String value) {

		String key = "my_jasypt_key";
		StandardPBEStringEncryptor pbeEnc = new StandardPBEStringEncryptor();
		pbeEnc.setAlgorithm("PBEWithMD5AndDES");
		pbeEnc.setPassword(key);
		return pbeEnc.encrypt(value);
	}
	

}